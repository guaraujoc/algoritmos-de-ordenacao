/*
Gustavo Carvalho Araújo
*/
#define M_SIZE 100000

typedef int numbers;

typedef struct {

    numbers *data;
    long size;

} list;

void alloc(list *l);
void free_l(list *l);
int insert(list *l, numbers x);
void print(list l);
void radix_sort(list l);
void bubble_sort(list l);
void bubble_sort_rev(list l);
void optimized_bubble_sort(list l);
void quicksort(list l, long start, long stop);
void heapsort(list l);
