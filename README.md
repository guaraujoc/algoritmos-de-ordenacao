# Sorting Algorithms

Author: Gustavo Carvalho Araujo

You can reach me through my LinkedIn profile: [Gustavo Araújo](https://www.linkedin.com/in/guaraujoc/)

## Description

This algorithm showcases various sorting functions implemented in C. Additionally, it generates a list of random values and, upon execution, prints the time taken for sorting. This allows for easy comparison between different sorting methods.

To test how a specific function behaves in the main program, uncomment it and make sure it's the only one uncommented. This way, the algorithm will exclusively execute that function and provide you with the relevant data.

The program is divided into functions that can operate independently and be used in other applications. Feel free to utilize them in your own projects.

## Usage

1. Clone the repository to your local machine.
2. Compile the program using a C compiler.
3. Uncomment the sorting function you want to test in the main program.
4. Run the program to observe sorting performance and timing results.

## Functions

The following sorting functions are available:

- `bubbleSort`: Implements the Bubble Sort algorithm.
- `insertionSort`: Implements the Insertion Sort algorithm.
- `selectionSort`: Implements the Selection Sort algorithm.
- `mergeSort`: Implements the Merge Sort algorithm.
- `quickSort`: Implements the Quick Sort algorithm.
- `radixSort`: Implements the Radix Sort algorithm.

Each function is designed to efficiently sort a list of random values, and you can choose the one that best suits your specific requirements.


## Compilation Requirements

&nbsp;&nbsp;&nbsp;&nbsp;C Compiler compatible with C11 or higher standard.

## Project Structure

&nbsp;&nbsp;&nbsp;&nbsp;The project is structured as follows:

&nbsp;&nbsp;&nbsp;&nbsp;The file "main.c" contains the main function of the program.

&nbsp;&nbsp;&nbsp;&nbsp;The file "list.h" is a header file that defines data structures and function prototypes used in the program.

&nbsp;&nbsp;&nbsp;&nbsp;The file "list.c" contains the implementations of the functions defined in the header file "list.h."

&nbsp;&nbsp;&nbsp;&nbsp;Other source code files can be added to implement additional functionalities.

&nbsp;&nbsp;&nbsp;&nbsp;The code has been implemented using Doubly Linked Lists, which makes it highly compatible with adding new functions and providing functions for use in other codebases.

&nbsp;&nbsp;&nbsp;&nbsp;The "Makefile" contains instructions for compiling the program.

## Contributing

&nbsp;&nbsp;&nbsp;&nbsp;Contributions are welcome! If you have suggestions, improvements, or corrections, please feel free to open an issue or submit a pull request.

## Support

&nbsp;&nbsp;&nbsp;&nbsp;Email: ggustavo2001@gmail.com

&nbsp;&nbsp;&nbsp;&nbsp;LinkedIn profile: [Gustavo Araújo](https://www.linkedin.com/in/guaraujoc/) 

## License

&nbsp;&nbsp;&nbsp;&nbsp;This program is licensed under the [MIT License](https://opensource.org/licenses/MIT).
