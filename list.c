/*
Gustavo Carvalho Araújo
*/

#include "list.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

// aloca memória para a lista
void alloc(list*l){

    l->data = malloc(M_SIZE * sizeof(numbers));
    l->size = 0;

}

//libera a memória da lista
void free_l(list*l){

    free(l->data);
    l->size = 0;

}

//insere elementos na lista
int insert(list*l, numbers x) {

    if(l->size == M_SIZE){ // determinar o tamanho da lista (A[n])
        return 1;
    }

    l->data[l->size] = x; //preenche a lista
    l->size++; //avança uma posição

    return 0;

}

// código auxiliar para imprimir a lista e verificar a funcionalidade do programa
int print_aux = 1;
void print(list l) {

    printf("Lista %d: [", print_aux);

    for (long i = 0; i < l.size; i++) {

        printf("%d, ", l.data[i]);

    }

    printf("\b\b]\n");

    print_aux++;
}

// subrotina do Radix_Sort
void counting_sort(list l, long position){

    // inicializa B com zeros
    int B[10] = {0};

    // verifica a ocorrência de cada algarismo e armazena em B
    for(long i = 0; i < l.size; i++){

        long key = (l.data[i]/position);
        key = key % 10;
        B[key] = B[key] + 1;

    }

    // calcula a posição dos algarismos
    for(long i = 1; i < 10; i++) {

        B[i] = B[i] + B[i - 1];

    }

    long C[l.size];

    // preenche C com os algarismos ordenados corretamente
    for (long i = l.size - 1; i >= 0; i--) { //retorna ao valor original

        long key = l.data[i]/position;
        key = key % 10;
        B[key] = B[key] - 1;
        C[B[key]] = l.data[i]; //salva o valor original

    }

    // transcreve os valores em C para a lista original
    for(long i = 0; i < l.size; i++) {

        l.data[i] = C[i];    // insere o elemento original na lista

    }

}

//função auxiliar para encontrar maior valor da lista
int bigger(list l){

    int big = l.data[0];

    for(int i = 1; i < l.size; i++){  //faz busca pelo maior valor a um custo O(n).
        if(l.data[i] > big) {
            big = l.data[i];

        }
    }
    return big;
}
//função principal do Radix_Sort
void radix_sort(list l) {

    int big = bigger(l); //determina o maior elemento possível
    long position = 1;

    //caminha pelas casas decimais da direita para a esquerda
    while((big/position) > 0) {
        counting_sort(l, position); // ordena os elementos pelo algarismo correspondente
        position = position * 10; // avança uma casa decimal
    }
}

//implementação do bubble sort
void bubble_sort(list l) {

    numbers x;

    for (long i = 0; i < l.size - 1; i++) {  //percorre a lista
        for (long j = 0; j < l.size - 1; j++) { //compara j a j
            if (l.data[j] > l.data[j + 1]) {   // caso o valor de l.size[j] seja maior que o próximo, troca

                // troca

                x = l.data[j];
                l.data[j] = l.data[j + 1];
                l.data[j + 1] = x;

            }

        }
    }
}

// função utilizada para análise de eficiência de algoritmos em ordem decrescente
void bubble_sort_rev(list l) {

    numbers x;

    for (long i = 0; i < l.size - 1; i++) { //percorre a lista
        for (long j = 0; j < l.size - 1; j++) {
            if (l.data[j] < l.data[j + 1]) {  //verifica se o valor próximo é menor que o valor de l.size[j]

                //troca

                x = l.data[j];
                l.data[j] = l.data[j + 1];
                l.data[j + 1] = x;

            }

        }
    }
}

//implementação do bubble sort otimizado
void optimized_bubble_sort(list l) {

    numbers x;
    bool sorted = true;  // função da stdbool para atribuir um valor booleano ao sorted

    for (long i = 0; i < l.size - 1; i++) {     //percorre a lista
        for (long j = 0; j < l.size - 1; j++) {  // compara j a j
            if (l.data[j] > l.data[j + 1]){  // se for, realiza a troca

                sorted = false;  //altera o valor booleano

                x = l.data[j];
                l.data[j] = l.data[j + 1];
                l.data[j + 1] = x;
            }
        }
        if(sorted == true) {  // verifica se o valor já está ordenado
            break;
        }
        sorted = true; // altera o valor booleano
    }
}

//subrotina do quicksort
int partition(list l, long start, long stop) {

    numbers x;
    long piv = l.data[stop]; // coloca um pivô na ponta da lista
    long i = start - 1; // coloca um pivô no início da lista

    for (long j = start; j < stop; j++) { // percorre j linearmente

        if (l.data[j] < piv) {   //compara o valor com o do pivô

            i = i + 1;        // caminha uma casa

            //troca

            x = l.data[i];
            l.data[i] = l.data[j];
            l.data[j] = x;

        }
    }

    // troca i + 1 com o número no final da lista
    x = l.data[i + 1];
    l.data[i + 1] = l.data[stop];
    l.data[stop] = x;

    return (i + 1); //devolve i + 1
}

//subrotina do quicksort
int random_partition(list l, long start, long stop) {

    numbers x;

    long k = rand() % (stop - start + 1) + start; // seleciona um valor aleatório entre stop e start

    //troca a posição k com a saída
    x = l.data[k];
    l.data[k] = l.data[stop];
    l.data[stop] = x;

    //chama a subrotina partition para ordenar l
    return partition(l, start, stop);

}

//implementação quicksort com particionamento de lomuto (permite duplicatas)
void quicksort(list l, long start, long stop){

    if (start < stop){   //verificar se o início é menor que o fim
        long piv = random_partition(l, start, stop); // ordena um pivô
        quicksort(l, start, piv - 1); //aplica a estratégia dividir para conquistar, com uma chamada recursiva
        quicksort(l, piv + 1, stop);
    }

}

void heapify(list l, long i) { // rearranja o heap

    numbers x;
    long big = i;          // toma i como o maior valor do heap
    long left = 2*i + 1;   // organiza os valores para esquerda
    long right = 2*i + 2;  // organiza os valores para direita

    if(left < l.size && l.data[left]> l.data[big]) { //verifica se o valor a esquerda é maior que o de cima

        big = left;

    }

    if(right < l.size && l.data[right]> l.data[big]) { //verifica se o valor a direita é maior que o de cima

        big = right;

    }

    if(big != i) { // verifica se o número de cima é diferente do maior valor que entrou

        x = l.data[i];
        l.data[i] = l.data[big];
        l.data[big] = x;

        heapify(l, big);  //chama o rearranjo para o lado do "filho" para verificar se ele está posicionado corretamente

    }
}

//implementação do heapsort
void heapsort(list l) {

    numbers x;

    for(long i = (l.size/2) - 1; i >= 0; i--) { //divide o vetor em 2 e inicia o rearranjo daquela parte

        heapify(l, i);

    }
    for(long i = l.size - 1; i >= 1; i--) { //pega o primeiro valor do vetor e manda para o vetor ordenado

        //troca
        x = l.data[0];
        l.data[0] = l.data[i];
        l.data[i] = x;

        //caminha com a lista, posto que o último valor está ordenado
        l.size--;

        heapify(l, 0); //organiza a nova árvore

    }
}
        
    
