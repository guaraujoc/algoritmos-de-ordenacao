/*
Gustavo Carvalho Araújo
 */

#define REP 10
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "list.h"

int main() {

    clock_t start, sum;
    list l;

    srand(time(NULL));

    for (int i = 1; pow(10, i) <= M_SIZE; i++) { //cria a lista de um tamanho 10^i
        sum = 0; // inicia a soma de tempo
        for (int j = 0; j <= REP - 1; j++) { //cria repetições
            alloc(&l);

            for (long k = 0; k < pow(10, i); k++) {

                insert(&l, rand() % 10000); //insere elementos aleatório na lista

            }

            //radix_sort(l);   // ordena a lista para teste de ordenação crescente
            //bubble_sort_rev(l);  // ordena lista para teste de ordenação decrescente

            start = clock(); // inicializa a contagem do tempo

            //print(l); // imprime a lista antes de ordenar

            //quicksort(l,  0, l.size - 1);  // ativa o quicksort
            //bubble_sort(l);                // ativa o bubblesort
            //optimized_bubble_sort(l);      // ativa o bubblesort otimizado
            //radix_sort(l);                   // ativa o radix sort
            //heapsort(l);                   // ativa o heapsort

            //print(l); // imprime a lista ordenada

            sum = sum + (clock() - start);  // calcula o tempo para a ordenação

            free_l(&l); //destrói a lista
        }

        // imprime o tempo necessário para ordenar 10^i elementos
        printf("Elementos ordenados: %.0lf \t Tempo: %10.5lf segundos\n",pow(10, i), (((double) sum) / REP) / CLOCKS_PER_SEC);
    }
    return 0;
}
